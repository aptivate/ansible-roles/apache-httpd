[![pipeline status](https://git.coop/aptivate/ansible-roles/apache-httpd/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-roles/apache-httpd/commits/master)

# apache-httpd

A role to install Apache web server.

# Requirements

  * `with_mod_ssl`: Whether to install `mod_ssl` as well.
    * Defaults to `true`

  * `with_openssl`: Whether to install `openssl` as well.
    * Defaults to `true`

  * `apache_config_home`: Where your Apache configuration lives.
    * Defaults to `/etc/httpd/conf.d`.

# Dependencies

None.

# Example Playbook

```yaml
- hosts: localhost
  roles:
     - role: apache-httpd
```

# Testing

We use Linode for our testing here.

You'll need to expose the `LINODE_API_KEY` environment variable for that.

```
$ pipenv install --dev
$ pipenv run molecule test
```

# License

  * https://www.gnu.org/licenses/gpl-3.0.en.html

# Author Information

  * https://aptivate.org/
  * https://git.coop/aptivate
