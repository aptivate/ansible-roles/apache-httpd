def test_httpd_installed(host):
    httpd_package = host.package('httpd')

    assert httpd_package.is_installed


def test_mod_ssl_installed(host):
    mod_ssl_package = host.package('mod_ssl')

    assert mod_ssl_package.is_installed


def test_openssl_installed(host):
    openssl_package = host.package('openssl')

    assert openssl_package.is_installed


def test_httpd_is_running(host):
    httpd_service = host.service('httpd')

    assert httpd_service.is_running
    assert httpd_service.is_enabled


def test_httpd_confd_directory_exists(host):
    httpd_confd_directory = host.file('/etc/httpd/conf.d/')

    assert httpd_confd_directory.exists
    assert httpd_confd_directory.is_directory
